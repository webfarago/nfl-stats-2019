<?php

function app_name() {
    return 'NFL Statistics 2019';
}

function current_year() {
    return date('Y');
}

function asset($asset) {
    echo 'http://' . $_SERVER['HTTP_HOST'] . '/' . $asset;
}

function e($output) {
    echo $output;
}

function inc($section) {
    include_once 'layouts/' . $section;
}

function url($path_name = 'home') {
    if ($path_name == 'home') {
        echo 'http://' . $_SERVER['HTTP_HOST'] . '/';
    } else {
        echo 'http://' . $_SERVER['HTTP_HOST'] . '/' . $path_name . '.php';
    }
}

function is_selected($needle) {
    $haystack = $_SERVER['REQUEST_URI'];
    return strpos($haystack, $needle) !== false;
}


/**
 * printing out data and the name of the data
 */
function pr($data, $what = null)
{
    $data_name = is_null($what) ? "data" : $what;
    echo "<pre>";
    echo "<h2>" . $data_name . "</h2>";
    print_r($data); // or var_dump($data);
    echo "</pre>";
}
