<header>
    <div class="navbar">
        <nav>
            <ul>
                <li>
                    <a href=""><i class="fas fa-football-ball"></i></a>
                </li>
                <li class="<?php echo is_selected('teams') ? 'selected' : ''; ?>">
                    <a href="<?php url('teams'); ?>">teams</a>
                </li>
                <li class="<?php echo is_selected('players') ? 'selected' : ''; ?>">
                    <a href="<?php url('players'); ?>">players</a>
                </li>
            </ul>
        </nav>
        <nav>
            <ul>
                <li>
                    <a href="/signin.php">
                        <i class="fas fa-sign-in-alt"></i>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</header>