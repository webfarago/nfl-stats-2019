<?php

require '../src/bootstrap.php';
// pr($_SERVER['REQUEST_URI'], 'request uri');
// pr(is_selected('teams'), 'selected');
$page_title = 'teams';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php e($title); ?></title>
    <link rel="stylesheet" href="<?php e(asset('styles/public.css')); ?>">
</head>
<body>
    <!-- HEADER -->
    <?php inc('public/header.php'); ?>

    <!-- MAIN CONTENT -->
    <main>
       <h1><?php e($page_title); ?></h1>
    </main>

    <!-- FOOTER -->
    <?php inc('public/footer.php'); ?>
</body>
</html>
