<?php

require '../src/bootstrap.php';
// pr($_SERVER, 'server');
$page_title = 'nfl statistics';
$subtitle = '2019';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php e($title); ?></title>
    <link rel="stylesheet" href="<?php e(asset('styles/public.css')); ?>">
</head>
<body>
    <!-- HEADER -->
    <?php inc('public/header.php'); ?>

    <!-- MAIN CONTENT -->
    <main>
       <h1><?php e($page_title); ?> <small><?php e($subtitle); ?></small></h1>
    </main>

    <!-- FOOTER -->
    <?php inc('public/footer.php'); ?>
</body>
</html>
